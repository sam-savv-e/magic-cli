import re
import setuptools

version = "0.0.1"

setuptools.setup(
    name="magic-cli",
    version=version,
    author="Samuel Hornsey",
    description="Spell Checker CLI tool for html docs",
    url="https://sam-savv-e@bitbucket.org/sam-savv-e/magic-cli.git",
    author_email="samh@savv-e.com.au",
    entry_points={
        "console_scripts": ['magic=magic.cli:runner']
    },
    install_requires=[
        "beautifulsoup4>=4.6.3",
        "bs4>=0.0.1",
        "colorama>=0.4.1",
        "pyspellchecker>=0.2.2"
    ]
)