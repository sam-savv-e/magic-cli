# Magic Spell CLI

A python CLI spell checker for HTML documents.

## Installation

```console
> pip install git+https://sam-savv-e@bitbucket.org/sam-savv-e/magic-cli.git
```

```console
> pip install --upgrade git+https://sam-savv-e@bitbucket.org/sam-savv-e/magic-cli.git
```

## Usage

```console
> magic --file ./path/to/dir/*.html
```

The CLI also supports single file checking

```console
> magic --file ./path/to/dir/index.html
```