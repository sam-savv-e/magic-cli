'''
Spell and Grammer Check CLI Tool
'''
from .file_parser import parse
from .spell import check

import argparse
from colorama import init
import os

class CLI:
    def __init__(self, args=None):
        self.rootPath, self.regex = os.path.split(args.files)
        self._run()

    def _run(self):
        self._run_both()

    def _run_both(self):
        for text, file in parse(self.rootPath, self.regex):
            check(text, file)

def runner():
    init()

    parser = argparse.ArgumentParser(description='Magic! A Python cli spellchecker.')

    parser.add_argument('--file', dest='files', help='file or regex expression for files')
    parser.add_argument('--out', dest='out', help='out file for results')

    args = parser.parse_args()

    cli = CLI(args)