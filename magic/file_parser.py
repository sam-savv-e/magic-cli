from bs4 import BeautifulSoup
import fnmatch
import string
import os

def _get_files(directory, regex):
    for root, dirs, files in os.walk(directory):
        for filename in files:
            if fnmatch.fnmatch(filename, regex):
                yield os.path.join(root, filename)


def parse(directory, regex):
    for file in _get_files(directory, regex):
        soup = BeautifulSoup(open(file, encoding="utf-8").read(), 'html.parser')
        s = "".join((char for char in soup.get_text().lower().strip() if char not in string.punctuation))
        yield s, file