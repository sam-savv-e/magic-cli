'''
Spell check
'''
from spellchecker import SpellChecker
from colorama import Fore, Back, Style

def check(text, file):
    words = text.split()
    spell = SpellChecker()

    misspelled = spell.unknown(words)

    for word in misspelled:
        print(Fore.RED + '==== Spelling Error Found ====', Style.RESET_ALL)
        print('File:', file)
        print(Fore.YELLOW + 'word:', word)
        print(Fore.GREEN + 'Correction:', spell.correction(word))